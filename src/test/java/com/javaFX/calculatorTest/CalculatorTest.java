package com.javaFX.calculatorTest;

import com.javaFX.calculator.Calculator;
import com.javaFX.calculator.Display;
import org.junit.Before;
import org.junit.Test;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.TEN;
import static org.junit.Assert.*;
import java.math.BigDecimal;

public class CalculatorTest {
    private Calculator calculator ;
    private Display displayByTest = new DisplayByTest();

    @Before
    public void setUp() throws Exception {
        calculator = new Calculator(displayByTest);
    }
    @Test
    public void getBigDecimalDigitTest() throws Exception {
        //given
        displayByTest.setDisplayDigit("0");
        //when
        BigDecimal digit = calculator.getBigDecimalDigit();
        //then
        assertEquals(ZERO, digit);
    }

    @Test
    public void setBigDecimalDigitTest() throws Exception {
        //given
       BigDecimal digit =  new BigDecimal(20.0);
        //when
       calculator.setBigDecimalDigit(digit);
        //then
        assertEquals("20", displayByTest.getDisplayDigit());
    }

    @Test
     public void checkingSequenceOfDisplayOfDigitsTest() throws Exception {
        assertEquals("0", displayByTest.getDisplayDigit());
        calculator.digit("1");
        assertEquals("1", displayByTest.getDisplayDigit());
        calculator.digit("5");
        assertEquals("15", displayByTest.getDisplayDigit());
    }

    @Test
    public void clearingDisplayTest() {
        //given
        calculator.digit("6");
        assertTrue(calculator.isLastButtonWasDigit());
        //when
        calculator.clearingDisplay();
        //then
        assertFalse(calculator.isLastButtonWasDigit());
        assertEquals(ZERO, calculator.getBigDecimalDigit() );
    }

    @Test
    public void commaTest() throws Exception {
        //given
        assertEquals("0", displayByTest.getDisplayDigit());
        //when
        calculator.comma();
        //then
        assertEquals("0.", displayByTest.getDisplayDigit());
    }

    @Test
    public void checkTheUseOfTheComma() throws Exception {
        //given
        calculator.digit("0.3");
        //when
        calculator.comma();
        //then
        assertNotEquals("0.3.", displayByTest.getDisplayDigit());
    }

    @Test
    public void mathOperatorMethodTest() throws Exception {
        //given
        calculator.digit("7");
        calculator.mathOperatorMethod("+");
        calculator.digit("3");
        //when
        calculator.mathOperatorMethod("+");
        //then
        assertEquals(TEN, calculator.getBigDecimalDigit());
    }

    @Test
    public void calculationOfTheResultTest() throws Exception {
        //given
        calculator.digit("7");
        calculator.mathOperatorMethod("-");
        calculator.digit("1");
        //when
        calculator.calculationOfTheResult();
        //then
        assertEquals(new BigDecimal(6), calculator.getBigDecimalDigit());
        //when
        calculator.calculationOfTheResult();
        //then
        assertEquals(new BigDecimal(5), calculator.getBigDecimalDigit());
    }

    @Test
    public void calculationAddTest() {
        //given
        calculator.digit("1");
        calculator.mathOperatorMethod("+");
        calculator.digit("0");
        //when
        BigDecimal result = ONE.add(ZERO);
        //then
        assertEquals(ONE, result);
    }

    @Test
    public void calculationSubtractTest() {
        //given
        calculator.digit("1");
        calculator.mathOperatorMethod("-");
        calculator.digit("0");
        //when
        BigDecimal result = ONE.subtract(ZERO);
        //then
        assertEquals(ONE, result);
    }

    @Test
    public void calculationMultiplyTest() {
        //given
        calculator.digit("1");
        calculator.mathOperatorMethod("*");
        calculator.digit("10");
        //when
        BigDecimal result = ONE.multiply(TEN);
        //then
        assertEquals(TEN, result);
    }

    @Test
    public void calculationMultiplyByZeroTest() {
        //given
        calculator.digit("1");
        calculator.mathOperatorMethod("*");
        calculator.digit("0");
        //when
        BigDecimal result = ONE.multiply(ZERO);
        //then
        assertEquals(ZERO, result);
    }

    @Test
    public void calculationDivideTest() {
        //given
        calculator.digit("10");
        calculator.mathOperatorMethod("/");
        calculator.digit("1");
        //when
        BigDecimal result = TEN.divide(ONE);
        //then
        assertEquals(TEN, result);
    }

    @Test
    public void calculationZeroDivideByDigitTest() {
        //given
        calculator.digit("0");
        calculator.mathOperatorMethod("/");
        calculator.digit("1");
        //when
        BigDecimal result = ZERO.divide(ONE);
        //then
        assertEquals(ZERO, result);
    }

    private static class DisplayByTest implements Display {
        private String displayDigit = "0";
        @Override
        public String getDisplayDigit() {
            return displayDigit;
        }

        @Override
        public void setDisplayDigit(String displayDigit) {
            this.displayDigit = displayDigit;
        }
    }
}
