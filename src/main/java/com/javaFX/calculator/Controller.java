package com.javaFX.calculator;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller implements Display {

    @FXML
    private TextField display;
    private Calculator calculator;

    public Controller() {
        calculator = new Calculator(this);
    }

    public void clearButtonClick(ActionEvent actionEvent) {
        calculator.clearingDisplay();
    }

    public void buttonDigitClick(ActionEvent actionEvent) {
        Button button = (Button)actionEvent.getSource();
        String digitString = button.getText();
        calculator.digit(digitString);
    }

    public void buttonComaClick(ActionEvent actionEvent) {
        calculator.comma();
    }

    public void buttonMathOperatorClick(ActionEvent actionEvent) {
        Button button = (Button)actionEvent.getSource();
        String mathOperatorString = button.getText();
        calculator.mathOperatorMethod(mathOperatorString);
    }

    public void buttonEquallyClick(ActionEvent actionEvent) {
        calculator.calculationOfTheResult();
    }

    @Override
    public String getDisplayDigit() {
        return display.getText();
    }

    @Override
    public void setDisplayDigit(String displayDigit) {
       display.setText(displayDigit);
    }

}
