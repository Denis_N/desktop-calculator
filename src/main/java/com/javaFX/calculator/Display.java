package com.javaFX.calculator;

public interface Display {
    String getDisplayDigit();
    void setDisplayDigit(String displayDigit);
}
